// ==UserScript==
// @name         Tribalwars Autoattack
// @namespace    piomar.me
// @version      1.2.0
// @description  Set up timer to attack at the given time
// @author       piomar123
// @match        https://*.plemiona.pl/game.php?village=*&screen=place&try=confirm
// @match        https://*.tribalwars.works/game.php?village=*&screen=place&try=confirm
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function format_time(clock, millis) {
        return `${clock} + ${millis} ms`;
    }

    var autoattackNode = document.createElement('div');

    var timeInput = document.createElement('input');
    timeInput.setAttribute('id', 'timeInput');
    timeInput.setAttribute('type', 'text');
    timeInput.setAttribute('value', window.localStorage.getItem('autoattackTimeStorage') || '08:00:11');
    timeInput.setAttribute('size', '8');
    timeInput.setAttribute('maxLength', '8');
    autoattackNode.appendChild(timeInput);
    autoattackNode.appendChild(document.createTextNode(' + '));

    var millisInput = document.createElement('input');
    millisInput.setAttribute('id', 'timeInput');
    millisInput.setAttribute('type', 'number');
    millisInput.setAttribute('value', window.localStorage.getItem('autoattackMillisStorage') || '200');
    millisInput.setAttribute('class', 'unitsInput');
    autoattackNode.appendChild(millisInput);
    autoattackNode.appendChild(document.createTextNode('ms'));

    var autoattackButton = document.createElement("button");
    autoattackButton.innerHTML = "Nakurwiaj Autoattack";
    autoattackButton.setAttribute('id', 'autoattackButton');
    autoattackButton.setAttribute('style', 'margin:10px;');
    autoattackButton.setAttribute('class', 'btn');
    autoattackNode.appendChild(autoattackButton);

    autoattackButton.onclick = function() {
        autoattackButton.setAttribute('disabled', 'true');
        timeInput.setAttribute('disabled', 'true');
        millisInput.setAttribute('disabled', 'true');

        var desired_arrival_time = timeInput.value;
        var millis = millisInput.value;
        var arrival_elem = document.querySelector('#date_arrival .relative_time') || console.log("Nie znaleziono tekstu czasu przybycia");
        var button = document.getElementById('troop_confirm_go') || console.log("Nie znaleziono przycisku ataku");
        var time = document.getElementById('serverTime') || console.log("Nie znaleziono czasu serwera");
        var title_elem = document.querySelector('#content_value h2')

        var observer;
        observer = new MutationObserver(function(mutations) {
            title_elem.innerHTML = `<span style='color:brown;'>[Autoatak na ${format_time(desired_arrival_time, millis)}]</span>`;
            if (arrival_elem.innerHTML.indexOf(desired_arrival_time) !== -1) {
                observer.disconnect();
                setTimeout(() => button.click(), millis);
            }
        });
        observer.observe(time, {childList: true});
        window.localStorage.setItem('autoattackTimeStorage', desired_arrival_time);
        window.localStorage.setItem('autoattackMillisStorage', millis);

        console.log("Aktualny czas serwera: " + time.innerHTML);
        console.log("Tekst w Przybyciu: " + arrival_elem.innerHTML);
        console.log("Przycisk do ataku: " + button.value);
        console.log("Wysyłam atak gdy będzie: " + format_time(desired_arrival_time, millis));
    };

    var attackForm = document.getElementById('command-data-form');
    attackForm.parentNode.insertBefore(autoattackNode, attackForm);

})();
