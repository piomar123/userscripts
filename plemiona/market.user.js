// ==UserScript==
// @name         Tribalwars Premium Exchange Notifier
// @namespace    piomar.me
// @version      1.0.0
// @description  Open a new window when PP for exchange are available
// @author       piomar123
// @match        https://*.plemiona.pl/game.php?village=*&screen=market&mode=exchange*
// @match        https://*.tribalwars.works/game.php?village=*&screen=market&mode=exchange*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function hasFreeSpace(stocks) {
        for (const stock of stocks) {
            if (!stock.classList.contains('warn')) {
                return true;
            }
        }
        return false;
    }

    const WINDOW_NAME = 'alert' + document.location.href.substring(0, 16);
    if (window.name === WINDOW_NAME) {
        return;
    }

    let stocks = [document.querySelector('#premium_exchange_stock_wood'),
                  document.querySelector('#premium_exchange_stock_stone'),
                  document.querySelector('#premium_exchange_stock_iron')];

    let exchangeTitle = document.querySelector('#market_status_bar + h3');

    exchangeTitle.innerHTML = "Waiting for mutation";

    let observer = new MutationObserver(function(mutations) {
        exchangeTitle.innerHTML = "[WATCHING CHANGES | updated " + new Date().toString().substring(0, 24) + " ]";
        if (!hasFreeSpace(stocks)) {
            return;
        }
        exchangeTitle.innerHTML = '<span style="color:red;">FIRED A NEW WINDOW. PLEASE REFRESH</span>';
        window.open(document.location, WINDOW_NAME, 'width=1000,height=800');
        observer.disconnect();
    });

    let config = { attributes: true, childList: true, characterData: true };
    for (let stock of stocks) {
        observer.observe(stock, config);
    }


})();