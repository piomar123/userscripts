// ==UserScript==
// @name         Tribalwars Build Queue Cut Time
// @namespace    piomar.me
// @version      1.0.0
// @description  Click free finish build if available
// @author       piomar123
// @match        https://*.plemiona.pl/game.php?*
// @match        https://*.tribalwars.works/game.php?*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    var observer = new MutationObserver((mutations) => {
        var btn = document.querySelector('a.btn-instant-free');
        if (!btn || btn.style.display == 'none' || btn.classList.contains('x-clicked')) { return; }
        btn.classList.add('x-clicked');
        setTimeout(() => btn.click(), 600);
    });

    observer.observe(document.getElementById('content_value'),
                     {attributes: true, childList: true, subtree: true});
})();