// ==UserScript==
// @name         Tribalwars Militia Wedge
// @namespace    piomar.me
// @version      1.1.0
// @description  Start farm militia at the given time
// @author       piomar123
// @match        https://*.plemiona.pl/game.php?village=*&screen=farm
// @match        https://*.tribalwars.works/game.php?village=*&screen=farm
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function format_time(clock, millis) {
        return `${clock} + ${millis} ms`;
    }

    function findOkButton() {
        return document.querySelector('button.btn-confirm-yes');
    }

    var autoMilitiaNode = document.createElement('div');

    var timeInput = document.createElement('input');
    timeInput.setAttribute('id', 'timeInput');
    timeInput.setAttribute('type', 'text');
    timeInput.setAttribute('value', window.localStorage.getItem('autoMilitiaTimeStorage') || '08:00:11');
    timeInput.setAttribute('size', '8');
    timeInput.setAttribute('maxLength', '8');
    autoMilitiaNode.appendChild(timeInput);
    autoMilitiaNode.appendChild(document.createTextNode(' + '));

    var millisInput = document.createElement('input');
    millisInput.setAttribute('id', 'timeInput');
    millisInput.setAttribute('type', 'number');
    millisInput.setAttribute('value', window.localStorage.getItem('autoMilitiaMillisStorage') || '500');
    millisInput.setAttribute('class', 'unitsInput');
    autoMilitiaNode.appendChild(millisInput);
    autoMilitiaNode.appendChild(document.createTextNode('ms'));

    var autoMilitiaButton = document.createElement("button");
    autoMilitiaButton.innerHTML = "Nakurwiaj Auto chłopów";
    autoMilitiaButton.setAttribute('id', 'autoMilitiaButton');
    autoMilitiaButton.setAttribute('style', 'margin:10px;');
    autoMilitiaButton.setAttribute('class', 'btn');
    autoMilitiaNode.appendChild(autoMilitiaButton);

    autoMilitiaButton.onclick = function() {
        autoMilitiaButton.setAttribute('disabled', 'true');

        var desired_arrival_time = timeInput.value;
        var millis = millisInput.value;
        findOkButton() || alert("Nie znaleziono przycisku stawiania zagrody");
        var time = document.getElementById('serverTime') || console.log("Nie znaleziono czasu serwera");
        var title_elem = document.querySelector('#content_value h2');

        var observer;
        observer = new MutationObserver(function(mutations) {
            title_elem.innerHTML = `[Auto zagroda gdy czas serwera ${format_time(desired_arrival_time, millis)}]`;
            if (time.innerHTML.indexOf(desired_arrival_time) !== -1) {
                setTimeout(() => findOkButton().click(), millis);
                observer.disconnect();
            }
        });
        observer.observe(time, {childList: true});
        window.localStorage.setItem('autoMilitiaTimeStorage', desired_arrival_time);
        window.localStorage.setItem('autoMilitiaMillisStorage', millis);

        console.log("Aktualny czas serwera: " + time.innerHTML);
        console.log("Przycisk do stawiania: " + findOkButton().innerHTML);
        console.log("Stawiam gdy czas serwera: " + format_time(desired_arrival_time, millis));
    };

    var commandDetailsForm = document.getElementById('content_value');
    if (commandDetailsForm) {
        commandDetailsForm.appendChild(autoMilitiaNode);
    } else {
        console.log("Command details form not found");
    }

})();
